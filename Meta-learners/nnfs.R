nnfs <- function(x, y, fam="binomial", maxit=1000, trace=FALSE){
  
  # Performs 'nonnegative forward search' (nnfs), a greedy algorithm to find a GLM 
  # where all regression coefficients are nonnegative, using the standard glm() function.
  # It is a version of AIC-based forward stepwise feature selection with the following
  # modification: If the addition of a feature would cause any regression coefficient
  # (including its own) to become negative, then the feature is removed from the list
  # of candidate features.
  
  # Input
  #   x: a numeric matrix of features
  #   y: a response vector
  #   fam: the GLM family
  #   maxit: the maximum number of iterations
  #   trace: whether to print progress to the console
  
  # Output
  #   a fitted model of class "glm"
  
  dat <- data.frame(y,x)
  step <- glm(y ~ 1, family = fam, data=dat)
  scope_vars <- names(dat)[-1]
  full_model <- as.formula(paste("y ~ ", paste(scope_vars, collapse= "+")))
  
  for(i in 1:maxit){
    
    candidate <- stepAIC(object = step, scope = full_model, direction = "forward", trace = trace, steps = 1)
    
    if(length(setdiff(names(candidate$model), names(step$model))) == 0){
      break()
    }
    else if(all(coef(candidate)[-1] >= 0)){
      step <- candidate
    }
    else{
      added_var <- tail(names(candidate$model),1)
      scope_vars <- setdiff(scope_vars, added_var)
      if(length(scope_vars) == 0){
        break()
      }
      else full_model <- as.formula(paste("y ~ ", paste(scope_vars, collapse= "+")))
    }
    
  }
  
  if(i == maxit){
    warning("Maximum number of iterations reached.")
  }
  return(step)
}
