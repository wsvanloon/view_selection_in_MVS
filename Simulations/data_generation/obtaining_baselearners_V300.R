### Obtaining the StaPLR coefficients and cross-validated predictions for new conditions featuring more correlation structures
### (based on Paper Experiment 4)
### Uses a function to generate train and test sets with super assignment

### Sorts the conditions by the number of features, so that the first 600 conditions have m=250, and the last 600 have m=2500.
rm(list=ls())

# Experimental Factors:
# Sample size: 200 or 2000
# Signal percentage: 0 (20 domains); 0.5 (5 domains); 1 (5 domains)
# Number of features per group: 250 or 2500
# Correlation Structure: - rw = 0.1, rb = 0
#                        - rw = 0.4, rb = 0
#                        - rw = 0.4, rb = 0.2

# Fixed Factors:
# Number of views: 30
# Number of signal groups: 10
# Effect size: 0.04
# Number of replicates: 100

# Experimental Conditions
NVIEWS <- 300
NFEATURES <- c(25, 250) # (per view)
SAMPSIZE <- c(200, 2000)
NSIGNAL <- list(c(rep(0, 290), rep(0.5, 5), rep(1, 5))) # change this when number of views changes
THETA <- c(0.04*sqrt(10))
CORS <- list(c(0.1, 0), c(0.5, 0), c(0.9, 0), c(0.5, 0.4), c(0.9, 0.4), c(0.9, 0.8))
ALPHABASE <- 0
ALPHAMETA <- 1
STDBASE <- TRUE #
STDMETA <- FALSE
BASEBOUNDL <- -Inf
BASEBOUNDU <- Inf
METABOUNDL <- 0 # non-negativity constraints: yes (0) / no (-Inf)
METABOUNDU <- Inf
CVLOSS <- "deviance"
CVLAMBDA <- "lambda.min"
CVPARA <- FALSE
LAMBRAT <- 0.0001
MYSEED <- NA
NORMALIZE <- TRUE
reps <- 1:100

conds <- expand.grid(rep=reps, v=NVIEWS, mbase=NFEATURES, n=SAMPSIZE, s=NSIGNAL, theta=THETA,
                     cors=CORS, alpha1=ALPHABASE, alpha2=ALPHAMETA, stdbase=STDBASE,
                     stdmeta=STDMETA, ll1=BASEBOUNDL, ul1=BASEBOUNDU, ll2=METABOUNDL,
                     ul2=METABOUNDU, cvloss=CVLOSS, cvlambda=CVLAMBDA, cvparallel=CVPARA,
                     lambdaratio=LAMBRAT, myseed=MYSEED, normalize=NORMALIZE)

# Generate Seed List
set.seed(210318)
seed.list <- sample(.Machine$integer.max, size = nrow(conds))
conds$seed <- seed.list
conds <- conds[order(conds$mbase), ]

# Simulation Function
sim_baselearners <- function(tid, arguments){
  
  # Load Required Libraries
  library(multiview)
  
  # Disallow early path termination
  glmnet.control(fdev=0)
  
  # Source Required Scripts (Shark)
  source("blockcorrelate.R")
  source("sim_normal_views.R") 
  
  argument <- arguments[tid, ]
  
  v <- argument$v
  mbase <- argument$mbase
  n <- argument$n
  s <- unlist(argument$s)
  theta <- argument$theta
  cors <- unlist(argument$cors)
  rw <- cors[1]
  rb <- cors[2]
  alpha1 <- argument$alpha1
  alpha2 <- argument$alpha2 
  myseed <- argument$myseed
  std.base <- argument$stdbase
  std.meta <- argument$stdmeta
  ll1 <- argument$ll1
  ul1 <- argument$ul1
  ll2 <- argument$ll2
  ul2 <- argument$ul2
  cvloss <- as.character(argument$cvloss)
  cvlambda <- as.character(argument$cvlambda)
  cvparallel <- argument$cvparallel
  normalize <- argument$normalize
  lambdaratio <- argument$lambdaratio
  
  # Simulate Data
  set.seed(argument$seed)
  
  m <- rep(mbase, v)
  
  randomizer <- sample(1:v)
  
  dat <- sim_normal_views(v=v, m=m[randomizer], ntrain=n, ntest=1000, s=s[randomizer], theta=theta, rw=rw, rb=rb)
  gc()
  view_index <- rep(1:v, m[randomizer])
  
  ## Start Timer
  timer0 <- proc.time()[3]
  
  # SDL Domain Selection
  fit <- StaPLR(x=dat$xtrain, y=dat$ytrain, view=view_index, alpha1=alpha1, alpha2=alpha2,
                nfolds=10, std.base=std.base, std.meta=std.meta, ll1=ll1, ul1=ul1,
                ll2=ll2, ul2=ul2, cvlambda=cvlambda, cvparallel=cvparallel, lambda.ratio=lambdaratio)
  
  ## Stop Timer
  timer1 <- proc.time()[3]
  timer <- round(timer1 - timer0)
  seconds <- timer %% 60
  minutes <- (timer %/% 60) %% 60
  hours <- (timer %/% 60) %/% 60
  timer <- c(hours, minutes, seconds)
  names(timer) <- c("hours", "minutes", "seconds")
  
  # Create output object
  out <- list("data"=dat[c(2:9)], "coefs"=coef(fit, cvlambda=cvlambda), "CVs"=fit$CVs, "timer"=timer)
  
  return(out)
}


# Perform Simulations
TID <- as.numeric(Sys.getenv("SGE_TASK_ID"))
results <- list()
results$param <- conds[TID,]
results$out <- sim_baselearners(tid = TID, arguments = conds)
save(results, file=paste0(TID, ".RData"))
warnings()


